const express = require ("express");
const { checkPendingOrder } = require("../middlewares/orders");
const { allOrders } = require("../models/Orders");
const router = express.Router();

router.post ('/', function (req, res) {
    const userid = req.headers.userid
    const PayMethod = req.body.PayMethod
    const Address = req.body.Address

    const userAllOrders = allOrders.filter ( order => order.UserId === userid)
    const checkoutOrder = userAllOrders.filter(order => order.Status === "Pending")
    
    checkoutOrder[0].PayMethod = PayMethod
    checkoutOrder[0].Address = Address
    checkoutOrder[0].Status = "Confirmed"

    res.status(200).json({Msj:`The order number ${checkoutOrder[0].OrderId} has been confirmed`})

    console.log("Checkout order es =", checkoutOrder)

})

module.exports = router