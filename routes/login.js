const express = require ("express");
const { login } = require("../middlewares/users");
const router = express.Router();

router.get ('/', function (req, res) {
    res.send('Login page')
});

router.post ('/', login, function (req, res) {

    res.send(`Welcome. Your user ID is ${req.myid}`)
    console.log('User logged in')    
})

module.exports = router