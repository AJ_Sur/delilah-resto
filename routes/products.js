const express = require ("express")
const router = express.Router();
const productsList = require ("../models/ProductsList")
const {validateProdID} = require ("../middlewares/products");
const { checkAdmin } = require("../middlewares/users");


router.get('/', function (req, res) {
    res.json(productsList);
});

router.post('/',checkAdmin, function (req, res) { 
    if (!req.isAdmin){ 
        res.status(401).json({Msj:"This user is not admin"})
    }else{
        newProduct = req.body
        newProduct.prodId=productsList.length+1
        newProduct.available=true
        productsList.push(newProduct)
        res.status(201).json({"New product crated" : newProduct})
    }
});

router.put('/',checkAdmin, validateProdID, function (req, res) { 
    if (!req.isAdmin){ 
        res.status(401).json({Msj:"This user is not admin"})
    }else{
    productsList[req.body.id].name=req.body.name
    productsList[req.body.id].price=req.body.price

    res.status(201).send('Product updated')
}});

router.delete('/',checkAdmin,validateProdID,  function (req, res) { 
    if (!req.isAdmin){ 
        res.status(401).json({Msj:"This user is not admin"})
    }else{
    productsList.splice(req.body.id,1);
    res.send('Product deleted')
}});

module.exports = router