const express = require ("express");
const { checkID,emailCheck } = require("../middlewares/users");
const router = express.Router();
const { newUser, editUser, readUser, deleteUser } = require("../functions/users");
    

router.post('/',emailCheck, newUser)

router.get('/:userNumber',checkID, readUser)

router.put('/me',checkID, editUser)

router.delete('/:userNumber',checkID, deleteUser)

module.exports = router