const express = require ("express");
const { checkPendingOrder } = require("../middlewares/orders");
const { checkAdmin } = require("../middlewares/users");
const { Order, allOrders } = require("../models/Orders");
const { productsList } = require("../models/ProductsList");
const router = express.Router();

router.get('/',checkAdmin,function name(req, res) {
    if (req.isAdmin){
        res.send(allOrders) 
    }else { 
        userOrders = allOrders.filter( user => user.UserId === req.headers.userid)
        res.send(userOrders)
    }
    
})

//Create order
router.post('/',checkPendingOrder, function name(req, res) {
    const order = req.body.order 
    const UserId = req.headers.userid
    const orderId = new Date().getTime()
    const PayMethod = "To confirm"
    const Address = "To confirm"
    const Status = "Pending"
    const Hour = new Date()

    let thisOrder = []
    let TotalPay = 0
    
    //Creates list or products requested
    for (i=0; i< order.length; i++){
        newProdToAdd = productsList.filter( product => product.prodId === order[i].prodId)
        newProdToAdd[0].qty=order[i].qty
        thisOrder.push(newProdToAdd[0])
    }
    //Calculate total price of order
    for (i=0; i< thisOrder.length; i++){
        TotalPay = TotalPay + thisOrder[i].price * thisOrder[i].qty
    }

    let finalOrder = new Order (UserId, orderId, thisOrder, TotalPay, PayMethod, Address, Status, Hour)
    console.log(finalOrder)
    allOrders.push(finalOrder)
} )

router.put('/',checkAdmin )

router.delete('/',checkAdmin )

module.exports = router