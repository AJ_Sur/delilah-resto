const productsList = require ("../models/ProductsList")

function validateProdID(req, res, next) {
   
    if (productsList.length > req.body.id && req.body.id >= 0){
        id = req.body.id
        next();
    } else {
        id = req.body.id
        return res.status(500).send(`Product ID "${id}" doesnt exist`)
    }
};


module.exports = {validateProdID}