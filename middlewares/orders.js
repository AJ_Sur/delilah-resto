const { allOrders } = require("../models/Orders");

function checkPendingOrder(req, res, next) {
    const userid = req.headers.userid

    const userAllOrders = allOrders.filter ( order => order.UserId === userid)
    const pendingOrders = userAllOrders.find(order => order.Status === "Pending")

    if (pendingOrders){
        res.send("Confirm your pending order before create a new one")
    }else{
        next();
    }

} 

module.exports ={checkPendingOrder}