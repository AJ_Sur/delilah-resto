const { UserList } = require("../models/UserList");

function checkID(req, res, next) {
    const a = parseInt(req.params.userNumber,10)
    let userOk = false
    for (const user of UserList){
        if (a === user.Id){
            userOk = true
        }
    };
    if (userOk){ next() }
    else{res.json({"Error":`User ID ${a} No valid`})};
    
};

function emailCheck (req, res, next){
    const newEmail = req.body.Email;

    for (const User of UserList) {
        if (newEmail === User.Email) 
            {return res.status(400).json({Msj:"Email already registered"})
        }
    } 
    next ()
    //"!!"transforma los nulls, los vacíos y los ceros en un "false"
};

function checkAdmin(req, res, next) {
    let isAdmin = false
    const userId = parseInt(req.headers.userid)
    const searchedUser = UserList.find(user => user.Id === userId)
    if (searchedUser == undefined){
        res.status(404).json(
            {
                "Error": "User not found"
            }
        )
    }else{
        if (searchedUser.Admin){
            isAdmin = true
            req.isAdmin=isAdmin
            next()
        }else{
            isAdmin = false
            req.isAdmin=isAdmin
            next()
        }
    }
};

function login(req, res, next) {
    const UserLog = req.body.UserName
    const UserPass= req.body.Pass
    const searchedUser = UserList.find( user => user.UserName === UserLog && user.Pass === UserPass)
    
    if (searchedUser){
        req.myid=searchedUser.Id
        next();
    }else { 
        res.json ({
            message: "Incorrect loggin data"
        })}
};

module.exports = {checkID, emailCheck,checkAdmin,login}