const { UserList } = require("../models/UserList");

function newUser(req, res) {
    const newUser = req.body;
    UserList.push(newUser);
    newUser.Admin = false
    newUser.Id = UserList.length-1
    return res.status(200).json({"Msj":"New user created"});
};

function readUser(req,res) {
    a = req.params.userNumber
    for (user of UserList){
        if (user.Admin === true && user.Id == a) {
            msj = "Admin user"+JSON.stringify(UserList)
            return res.json({"Admin":"All users data", UserList})
        };
        if (user.Admin === false && user.Id == a){
            msj = "Sus datos personales son: "+JSON.stringify(user)
            return res.json({"User":"Your personal data", user}) };
    } 
};

function editUser(req, res) {
    console.log(`La ID a modificar es ${req.headers.userId}`)
        
        for (const user of UserList ) {
            if (req.params.userNumber == user.Id) 
            {   a = user.Id
                UserList[a].UserName=req.body.UserName
                UserList[a].Name=req.body.Name;
                res.json({"User modified":`ID ${a}`,user});
                break;
            }else {}
        } 
        res.send (`La ID: ${req.params.userNumber} no pertenece a un usuario`)
        
};

function deleteUser(req, res) {
    let userId = parseInt(req.params.userNumber)
    const user2Delete = UserList.findIndex((user)=> user.Id === userId)
    UserList.splice(user2Delete,1);
    res.json({'Msj':`ID ${userId} Delete successfully`})
};



//para productos
function validateID(req, res, next){
    if(req.body.id >= 0 && req.body.id < UserList){
        next()
    }else{
        return res.status(500).json("ID number invalid")
    }
    
};

module.exports = { newUser, readUser, editUser, deleteUser}

//Special thx to Vero y Diego