class Order {
    constructor (UserId, OrderId, Products, TotalPay, PayMethod, Address, Status, Hour) {
        this.UserId = UserId,
        this.OrderId = OrderId,
        this.Products = Products,
        this.TotalPay = TotalPay,
        this.PayMethod = PayMethod,
        this.Address = Address,
        this.Status = Status,
        this.Hour = Hour

    }
}

let allOrders = []

module.exports = {Order, allOrders}