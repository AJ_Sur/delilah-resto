productsList = [
    {
        name: "Test product A",
        price: 10.10,
        description: "Hardcoded test product number one",
        prodId: 1,
        available: true
    },
    {
        name: "Test product B",
        price: 20.20,
        description: "Hardcoded test product number two",
        prodId: 2,
        available: false
    },
    
]

module.exports = {productsList}