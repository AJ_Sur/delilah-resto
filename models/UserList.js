const UserList = [
    {UserName: "User00", Name:"Name00", Email: "Email@00", Phone: "0000", Address: "Address00", Pass: "Pass00", Id: 0, Admin: true},
    {UserName: "User01", Name:"Name01", Email: "Email@01", Phone: "0101", Address: "Address01", Pass: "Pass01", Id: 1, Admin: false},
    {UserName: "User02", Name:"Name02", Email: "Email@02", Phone: "0202", Address: "Address02", Pass: "Pass02", Id: 2, Admin: false}
]

module.exports = {UserList}