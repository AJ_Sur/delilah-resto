const express = require ("express")
const app = express();
app.use(express.json()); //Middleware para interpretar json

const login = require ('./routes/login.js')
app.use ('/login', login);

const users = require ('./routes/users.js')
app.use('/users', users); 
 
const products = require ('./routes/products.js')
app.use('/products', products); 

const orders = require ('./routes/orders.js')
app.use('/orders', orders);

const checkout = require ('./routes/checkout.js')
app.use('/checkout', checkout);


app.listen(3000, ()=> console.log("Server working on port 3000"));
